report({
  "testSuite": "BackstopJS",
  "tests": [
    {
      "pair": {
        "reference": "../bitmaps_reference/BackstopHighEdWeb2020Test_HEWeb_2020_Homepage_0_document_0_phone.png",
        "test": "../bitmaps_test/20221119-115719/BackstopHighEdWeb2020Test_HEWeb_2020_Homepage_0_document_0_phone.png",
        "selector": "document",
        "fileName": "BackstopHighEdWeb2020Test_HEWeb_2020_Homepage_0_document_0_phone.png",
        "label": "HEWeb 2020 Homepage",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://test-highedweb-2020-annual-conference.pantheonsite.io/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "phone",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/BackstopHighEdWeb2020Test_HEWeb_2020_Homepage_0_document_1_tablet.png",
        "test": "../bitmaps_test/20221119-115719/BackstopHighEdWeb2020Test_HEWeb_2020_Homepage_0_document_1_tablet.png",
        "selector": "document",
        "fileName": "BackstopHighEdWeb2020Test_HEWeb_2020_Homepage_0_document_1_tablet.png",
        "label": "HEWeb 2020 Homepage",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://test-highedweb-2020-annual-conference.pantheonsite.io/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "tablet",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/BackstopHighEdWeb2020Test_HEWeb_2020_Homepage_0_document_2_desktop.png",
        "test": "../bitmaps_test/20221119-115719/BackstopHighEdWeb2020Test_HEWeb_2020_Homepage_0_document_2_desktop.png",
        "selector": "document",
        "fileName": "BackstopHighEdWeb2020Test_HEWeb_2020_Homepage_0_document_2_desktop.png",
        "label": "HEWeb 2020 Homepage",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://test-highedweb-2020-annual-conference.pantheonsite.io/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/BackstopHighEdWeb2020Test_HEWeb_2020_Monday_Schedule_0_document_0_phone.png",
        "test": "../bitmaps_test/20221119-115719/BackstopHighEdWeb2020Test_HEWeb_2020_Monday_Schedule_0_document_0_phone.png",
        "selector": "document",
        "fileName": "BackstopHighEdWeb2020Test_HEWeb_2020_Monday_Schedule_0_document_0_phone.png",
        "label": "HEWeb 2020 Monday Schedule",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://test-highedweb-2020-annual-conference.pantheonsite.io/schedule/monday/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "phone",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/BackstopHighEdWeb2020Test_HEWeb_2020_Monday_Schedule_0_document_1_tablet.png",
        "test": "../bitmaps_test/20221119-115719/BackstopHighEdWeb2020Test_HEWeb_2020_Monday_Schedule_0_document_1_tablet.png",
        "selector": "document",
        "fileName": "BackstopHighEdWeb2020Test_HEWeb_2020_Monday_Schedule_0_document_1_tablet.png",
        "label": "HEWeb 2020 Monday Schedule",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://test-highedweb-2020-annual-conference.pantheonsite.io/schedule/monday/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "tablet",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/BackstopHighEdWeb2020Test_HEWeb_2020_Monday_Schedule_0_document_2_desktop.png",
        "test": "../bitmaps_test/20221119-115719/BackstopHighEdWeb2020Test_HEWeb_2020_Monday_Schedule_0_document_2_desktop.png",
        "selector": "document",
        "fileName": "BackstopHighEdWeb2020Test_HEWeb_2020_Monday_Schedule_0_document_2_desktop.png",
        "label": "HEWeb 2020 Monday Schedule",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://test-highedweb-2020-annual-conference.pantheonsite.io/schedule/monday/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    }
  ],
  "id": "Backstop HighEdWeb 2020 Test"
});